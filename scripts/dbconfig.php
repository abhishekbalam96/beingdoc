<?php
class Database
{   


    private $host = "localhost";
    private $db_name = "being_doc";
    private $username = "root";
    private $password = "";
    
    // private $host = "localhost";
    // private $db_name = "id6236921_being_doc";
    // private $username = "id6236921_root";
    // private $password = "czar2302";
    
    
    public $conn;
     
    public function dbConnection()
	{
     
	    $this->conn = null;    
        try
		{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
           
        }   
		catch(PDOException $exception)
		{
            echo "Connection error: " . $exception->getMessage();
        }
         
        return $this->conn;
    }
}
?>
